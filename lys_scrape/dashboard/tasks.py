# -*- coding: utf-8 -*-
import os
import time
import csv

from django.conf import settings

import requests
from bs4 import BeautifulSoup

# from lys_scrape.celery import app


# @app.task
def scrape_lys():
    """Celery task to scrape liveyoursport.com"""

    catalog_request_headers = {'Accept': 'application/json, text/javascript, */*; q=0.01'}
    dump_dir = settings.BASE_DIR + "/export/"

    if not os.path.exists(dump_dir):  # if directory doesn't exist, create
        os.makedirs(dump_dir)

    file_dump = dump_dir + "product_details_" + str(int(time.time())) + ".csv"

    base_url  = "https://www.liveyoursport.com/"
    category_url = base_url + "squash/"
    
    category_request = requests.get(category_url)
    category_soup = BeautifulSoup(category_request.text, 'html.parser')

    cat_id = category_soup.find('div', {'id': "CatId"}).text

    csvfile = open(file_dump, 'wb')
    csv_writer = csv.writer(csvfile, delimiter=",")

    csv_writer.writerow(["Name", "Price", "Description", "URL"])
    page = 1
    while True:
        catalog_url = base_url + "catalog?page={1}&limit=36&sort=featured&category={0}&is_category_page=1&facets=0".format(cat_id, page)
        catalog_request = requests.get(catalog_url, headers=catalog_request_headers)

        catalog_json = catalog_request.json()
        for product_data in catalog_json['variants']:
            product_url = product_data['product']['url']
            
            product_request = requests.get(product_url)
            product_soup = BeautifulSoup(product_request.text, 'html.parser')

            print product_url
            name = product_soup.find('h1').text
            price = product_soup.find('em', {'class': "ProductPrice VariationProductPrice"}).text
            description_div = product_soup.find('div', {'id': "ProductDescription"})
            description_span = description_div.find('span', {'class': "prod-descr"})
            description = description_span.text

            row = [name, price, description, product_url]
            csv_writer.writerow([unicode(s).encode('utf-8') for s in row])

        if catalog_json['meta']['pagination']['page_count'] <= page:
            break
        page += 1

    csvfile.close()