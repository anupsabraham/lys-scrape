# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from os import listdir
from os.path import isfile, join, basename, dirname

from django.shortcuts import render
from django.views.static import serve
from django.conf import settings

from .tasks import scrape_lys


def index(request):
    """
    Main page for the project
    """
    if request.method == "POST" and 'submit' in request.POST:
        scrape_lys()
        return 

    return render(request, 'index.html')

def files(request):
    """
    List the files and link to download
    """
    response_dict = {}
    dump_dir = settings.BASE_DIR + "/export/"
    files = [f for f in listdir(dump_dir) if isfile(join(dump_dir, f))]
    print files

    response_dict['files'] = files

    return render(request, 'files.html', response_dict)

def download_file(request, filename):
    """
    View to download file
    """
    dump_dir = settings.BASE_DIR + "/export/"
    filepath = dump_dir + filename

    return serve(request, basename(filepath), dirname(filepath))
